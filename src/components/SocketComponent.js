import React, { useEffect, useState } from "react";
import Pusher from "pusher-js";
import Echo from "laravel-echo";

const SocketComponent = () => {
  const [progressPercentage, setProgressPercentage] = useState(0);
  const [totalItem, setTotalItem] = useState(0);
  const [completeItem, setCompleteItem] = useState(0);
  const [processing, setProcessing] = useState(false);

  useEffect(() => {
    initializeEcho();
    // Clean up the Echo instance on component unmount
    return () => {
      window.Echo.disconnect();
      console.log("disconnect");
    };
  }, []);

  const initializeEcho = () => {
    window.Pusher = Pusher;
    window.Echo = new Echo({
      broadcaster: "pusher",
      key: "6eeb4f3291aff19ffbc2",
      cluster: "ap1",
      forceTLS: true,
      enabledTransports: ["ws", "wss"],
      // wsHost: "127.0.0.1",
      // wsHost: "translate-dev-api.loftal.jp",
      // wsPort: 6001,
      // encrypted: false,
      // wsPort: 6001,
      // wssPort: 6001,
      // disableStats: true,
      // transports: ["websocket"],
      // enabledTransports: ["ws"],
    });
  };

  const clickBtn = async () => {
    setProgressPercentage(0);
    setTotalItem(0);
    setCompleteItem(0);
    setProcessing(true); // Set processing to true when translation starts

    const requestData = {
      from_language_id: 1,
      to_language_id: 2,
      num_results: 1,
      conversations: [
        {
          character_id: 1,
          content: "What is your name",
          text_limit: 10,
          line_limit: 1,
        },
        {
          character_id: 1,
          content: "What is your name",
          text_limit: 10,
          line_limit: 1,
        },
        {
          character_id: 1,
          content: "What is your name",
          text_limit: 10,
          line_limit: 1,
        },
        {
          character_id: 1,
          content: "What is your name",
          text_limit: 10,
          line_limit: 1,
        },
      ],
    };

    try {
      //call create api
      const response = await fetch(
        // "http://127.0.0.1:8000/api/translates",
        "https://translate-dev-api.loftal.jp/api/translates",
        {
          method: "POST",
          headers: {
            Authorization:
              "Bearer 1|mcMXCpehbo1vS0k350ZyaxJHq2ki07xRyHc5nJTD4b98b3c8",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(requestData),
        }
      );

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const data = await response.json();

      //create response json
      document.getElementById("text").innerHTML =
        "Response: " + JSON.stringify(data);
      console.log(`translate.${data.translate.id}`);

      //list the event
      window.Echo.channel(`translate.${data.translate.id}`).listen(
        "TranslateProgressUpdate",
        (event) => {
          // Update the state with the progress_percentage from the event
          setProgressPercentage(event.progress_percentage);
          setTotalItem(event.total_items);
          setCompleteItem(event.complete_items);

          // Set processing to false when translation is complete
          if (event.progress_percentage === 100) {
            setProcessing(false);
          }

          // Rest of your code
          console.log("Received event:", event);
          // Handle the received event
        }
      );
    } catch (error) {
      console.error("Error:", error);
      document.getElementById("text").innerHTML = "Error occurred";
      setProcessing(false); // Set processing to false on error
    }
  };

  return (
    <div style={{ backgroundColor: "black", color: "white" }}>
      <h1>Socket</h1>
      <button onClick={clickBtn} disabled={processing}>
        {processing ? "Processing..." : "Translate"}
      </button>
      <div id="text">{/* Display progress_percentage from the state */}</div>
      {processing && <div>Processing translation...</div>}
      <div>Progress Percentage: {progressPercentage}</div>
      <div>
        Complete Progress: {completeItem}/{totalItem}
      </div>
    </div>
  );
};

export default SocketComponent;
